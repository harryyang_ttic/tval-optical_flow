function [ output_args ] = RunAll( folder_name )
%RUNALL Summary of this function goes here
%   Detailed explanation goes here
files=dir(strcat(folder_name,'/*.png'));
for i=1:numel(files)
    img1=imread('000200c.png');
    img2=imread('000201c.png');
    img1=rgb2gray(img1);
    img2=rgb2gray(img2);
    uv=estimate_flow_interface(img1,img2,'classic+nl-fast');
    uv1=uv(:,:,1);
    uv2=uv(:,:,2);
    dlmwrite('uv1.txt',uv1);
    dlmwrite('uv2.txt',uv2);
end
end

