function [ output_args ] = est_flow( imagename1,imagename2,outname1,outname2 )
%EST_FLOW Summary of this function goes here
%   Detailed explanation goes here
img1=imread(imagename1);
img2=imread(imagename2);
img1=rgb2gray(img1);
img2=rgb2gray(img2);
uv=estimate_flow_interface(img1,img2,'classic+nl-fast');
uv1=uv(:,:,1);
uv2=uv(:,:,2);
dlmwrite(outname1,uv1);
dlmwrite(outname2,uv2);

end

